# cpumemgraph

A simple tool that makes graphs of CPU and memory usage, ideal to monitor the performance of a
program and its effect on the OS.

It uses [top](https://gitlab.com/procps-ng/procps/-/tree/master/top) tool to gather CPU and memory
data, [GNU Awk](https://www.gnu.org/software/gawk/) as a scripting language that compiles data in a
plottable way, and [Gnuplot](http://www.gnuplot.info/) for plotting. It is used with GNU/Linux, but
may work on similar systems.

It provides statistics on available memory in the system, CPU core usage and total percent of time
spent in user/kernel/wait. The top tool gives these measurement every second. The number of cores
and type of data captured need to be adjusted from the awk script.

![example graph](sys.png "example output")

## Usage

This is the capture command:

```shell
$ while true ; do top -b -n1 -d1 -Em -p0 | awk -f log.awk ; sleep 1; done > sys.dat
```

This calls top every second, formats its data into a line that can be used for plotting in the
sys.dat file. Once the capture is done, this gnuplot command will process the file and create the sys.png
image:

```shell
$ gnuplot sys.plot
```

See the sys.plot file if you want to change the parameters of the graph generation, in particular the
first line, set `MAXMEM` to match your system's memory to have the correct scale for the memory graph.
The number of cores can be changed by duplicating the CPU lines, right after the `plot` command, but for
simplicity, three variants of the files are given, one for 4 cores, used in the examples above, 
one for 8 cores with an '8' in the file names, one for 16 cores with a '16' in the file names (for
capture too)..

**The format of top output changes with versions:** note that some versions of
top do not show the expected information by default. You can try this command
once, to check its output: `top -b -n1 -d1 -Em -p0`. If there is only one
statistics line for all CPU cores, or a wide line with two, you can change the
mode of operation of top using the series of keys
[documented in this issue](https://gitlab.com/free-astro/cpumemgraph/-/issues/1#note_352562427).

## Usage with siril

This tool was made specifically to work with [siril](https://gitlab.com/free-astro/siril). The
only change from the generic tool is that we can display the different commands being processed in
the graph, adding some context to what is seen in it.

The following command converts siril output to gnuplot commands that are loaded in a second
file:

```shell
$ grep '^1[0-9]*: ' log > commands.log
$ awk -f siril_log_to_dat.awk commands.log > labels.plot
```

The load of labels.plot must be uncommented in sys.plot, as it is optional,
before running the `gnuplot` command.

## Interpreting the graphs

The CPU usage graphs are drawn as cumulated area. If the background of the image show the four
colours as in the example image above, it means that all CPU cores are working. On what they are
working is a big question, and that's what the green, orange and red solid line curve aim to answer.

The green is the amount of user code executing, the higher it is, the more efficient the program is
in general. The orange curve is the amount of kernel code executing, in general as system calls,
like handling I/O. The red curve is the amount of time spent waiting for the requested I/O. When
it's high, threads are waiting for data instead of processing the data, and it's a loss of time that
may not easily be overcome, in particular if the device is slow compared to processing power.

The blue curve is the amount of *available* memory in the system. If it's low, it means that some
programs are using, if it gets too low, the system may crash or call the OOM reaper.
