# This AWK script reads the output of the top command given below and creates a
# single line of output per call suitable for graphing use.
#
# It gives: the data, the percent of use of the first 8 cpu cores, the sum of
# the uses of the 8 first cores in terms of user process, kernel process and
# I/O wait, the amount of memory available in MiB
#
# the command from which it gets the output:
# top -b1 -n1 -d1 -Em -p0

# gets the value in a string like "12.0 us", the first field before a space
function getval(str) { split(str, f, " "); return f[1] }

BEGIN { FS="[:,]"; USER=0; SYS=0; IO=0 }
# for the cpu lines, $2 is user, $3 is system, $4 is nice, $5 is idle, $6 is waiting I/O,
# $7 is time servicing hardware interrupts, $8 software ints, $9 hypervisor stolen time
/^%Cpu0/ { CPU0=(100 - getval($5)) ; USER+=getval($2); SYS=getval($3); IO=getval($6) }
/^%Cpu1/ { CPU1=(100 - getval($5)) ; USER+=getval($2); SYS=getval($3); IO=getval($6) }
/^%Cpu2/ { CPU2=(100 - getval($5)) ; USER+=getval($2); SYS=getval($3); IO=getval($6) }
/^%Cpu3/ { CPU3=(100 - getval($5)) ; USER+=getval($2); SYS=getval($3); IO=getval($6) }
/^%Cpu4/ { CPU4=(100 - getval($5)) ; USER+=getval($2); SYS=getval($3); IO=getval($6) }
/^%Cpu5/ { CPU5=(100 - getval($5)) ; USER+=getval($2); SYS=getval($3); IO=getval($6) }
/^%Cpu6/ { CPU6=(100 - getval($5)) ; USER+=getval($2); SYS=getval($3); IO=getval($6) }
/^%Cpu7/ { CPU7=(100 - getval($5)) ; USER+=getval($2); SYS=getval($3); IO=getval($6) }
/^MiB Swap/ { split($4, s, " "); MEM=s[3] }
END { print systime() " " CPU0 " " CPU1 " " CPU2 " " CPU3 " " CPU4 " " CPU5 " " CPU6 " " CPU7 " " USER " " SYS " " IO " " MEM }
